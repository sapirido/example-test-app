import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  name:string;
  years:number;
  salary:number;
  overYearError:string;
  userId:string;

  onSubmit(){
    if(this.isFormValid()){
      this.customerService.createCustomer(this.userId,this.name,this.years,this.salary)
      this.router.navigate(['/customers']);
    }
  }

  isFormValid(){
    if(this.years > 24 || this.years < 0){
      this.overYearError = "you need to type number between 0-24 "
      return false;
    }
    return true;
  }

  clearError(){
    this.overYearError = '';
  }

  ngOnInit():void{
    this.auth.getUser().subscribe(
    user => this.userId = user.uid
    )
  }
  constructor(public auth:AuthService,private customerService:CustomerService,private router:Router) { }



}
