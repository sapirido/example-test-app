import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Customer } from './interfaces/customer';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  private url = "https://dg2q9jwmc1.execute-api.us-east-1.amazonaws.com/beta"

  createCustomer(userId:string,name:string,years:number,salary:number){ 
    const ref = this.userCollection.doc(userId); //document specify from the users collection by userId
    const customer = {
      name:name,
      years:years,
      salary:salary
    }
    ref.collection('customers').add(customer); //go to his customers collection and push the new customer
  }


  deleteCustomer(userId:string, customerId:string){
    this.db.doc(`users/${userId}/customers/${customerId}`).delete();
    return this.customerCollection.snapshotChanges();
  }

  editCustomer(userId:string,customer:Customer){
    this.db.doc(`users/${userId}/customers/${customer.uid}`).update(
      {
     name:customer.name,
     years:customer.years,
     salary:customer.salary
      }
    );
  }

  updatePredict(userId:string,customerId:string,predict:string){
    this.db.doc(`users/${userId}/customers/${customerId}`).update(
      {
        predict:predict
      }
    );
  }


  public getAllCustomers(userId){
  this.customerCollection = this.userCollection.doc(userId).collection('customers');
  return this.customerCollection.snapshotChanges();
  }


  predictCustomer(years:number,income:number){ 
      let json = {
        'data':
        {
          'years':years,
          'income':income
        }
      }
      
      return this.http.post<any>(this.url,json).pipe(
        map( res => {
          return res;
        })
      );
  }

        // .then(res =>{
      //   if(res.statusCode === 200){
      //     console.log({res:res.body})
      //     return res.body;
      //     }else{
      //       throw new Error('something wrong with your body request or url.');
      //    }
      // })
      // .catch(err =>console.log('err',err))
  

  constructor(private db:AngularFirestore, private http:HttpClient) { }
}
