import { AuthService } from './../auth.service';
import { CustomerService } from './../customer.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import { Customer } from '../interfaces/customer';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  customers:Customer[] = [];
  customers$;
  panelOpenState:boolean = false;
  userId:string;
  predict:string;
  // isPredict:boolean = false;
  editstate = [];
  predictState =[];


  constructor(private router:Router,private customerService:CustomerService,public authService:AuthService) { }

  onCreateClicked(){
    this.router.navigate(['/customer/create']);
  }

  deleteCustomer(uid:string){
    
    this.customerService.deleteCustomer(this.userId,uid);
  }

  // editClicked(uid:string){
  //   this.customerService.editCustomer(this.userId,uid);
  // }

  updateCustomer(values:Customer){
    this.customerService.editCustomer(this.userId,values);
  }


  customerPredict(years,salary){
    this.predict = '';
    this.customerService.predictCustomer(years,salary).subscribe(
      res=> {
       this.predict = res.body;
      }
    );
  }

  updatePredict(customerId:string,predict:string){
    this.customerService.updatePredict(this.userId,customerId,predict);
  }
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user =>{
        this.userId = user.uid;
        this.customers$ = this.customerService.getAllCustomers(user.uid);
        this.customers$.subscribe(
          docs =>{
            this.customers = [];
            console.log(docs.length);
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              customer.uid = document.payload.doc.id;
              this.customers.push(customer);
            }
          }
        )
      }
    )

      // res.docs.forEach(doc => {
      //   if(doc.exists){
      //     console.log(doc.data());
      //   }
      // })
      
  
    // this.customers$.subscribe(
    //   docs => {
    //     console.log({docs})
    //     for(let doc of docs){
    //       console.log({data:doc.payload.doc.data()})
    //       const customer:Customer = doc.payload.doc.data();
    //       customer.uid = doc.payload.doc.id;
    //       this.customers.push(customer);
    //     }
    //   }
    // );
  }
}

