export interface Customer {
    uid?:string;
    name:string;
    years:number;
    salary:number;
    predict?:string;
}
