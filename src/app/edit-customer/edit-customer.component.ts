import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'editcustomer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
  @Input() name:string;
  @Input() years:number;
  @Input() salary:number;
  @Input() id:string;
  @Output() updateCustomer = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();


  constructor() { }

  editCustomer(){
    const customer:Customer = {uid:this.id,name:this.name,years:this.years,salary:this.salary};
    this.updateCustomer.emit(customer);
  }

  closeForm(){
    this.closeEdit.emit();
  }

  ngOnInit(): void {
  }

}
