// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBoDpOHH9S4snfdgYoqJqfiSGP0Pz78NQA",
    authDomain: "example-test-sapir.firebaseapp.com",
    projectId: "example-test-sapir",
    storageBucket: "example-test-sapir.appspot.com",
    messagingSenderId: "415635476087",
    appId: "1:415635476087:web:889f85ae20da4969128072"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
